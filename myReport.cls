\LoadClass[a4paper,11pt]{article}
\ProvidesClass{myReport}

%=======================================================================================================================================================================================================

%packages
\RequirePackage{graphicx,amsmath,multirow,pgfplots,url,subfigure}
\RequirePackage[english]{babel}
\RequirePackage[font=footnotesize,labelfont=bf]{caption}% for altering caption format
\RequirePackage[a4paper,left=2.3cm,right=2.3cm]{geometry}% can alter margins like this
\RequirePackage{kpfonts}
\RequirePackage[T1]{fontenc}
\RequirePackage{feynmp}% Feynman diagrams?
\RequirePackage{textcomp} %for underscore
\RequirePackage[bookmarks=true,pdffitwindow=true]{hyperref}

%=======================================================================================================================================================================================================

%formatting for equations, figs and tables
\renewcommand{\theequation}{\thesection.\arabic{equation}}\numberwithin{equation}{section}
\renewcommand{\thefigure}{\thesection.\arabic{figure}}\numberwithin{figure}{section}
\renewcommand{\thetable}{\thesection.\arabic{table}}\numberwithin{table}{section}

%=======================================================================================================================================================================================================

\newcommand{\maketitlepage}[4] {
	\begin{center}
	\thispagestyle{empty}
	\textbf{\LARGE{{#1}\\[20pt]}}
	\begin{normalsize}
	\today\\[20pt]
	\textbf{{#2}}$^1$\\[3pt]
	{#3}\\[10pt]
	\end{normalsize}
	\end{center}


	\begin{center}\begin{small}$^1$\textit{{#4}}
	\end{small}\end{center}

	\bigskip

}

%=======================================================================================================================================================================================================

\newcommand{\includeabstract}[1] {
	\IfFileExists{#1}{\input{#1}}
}

%=======================================================================================================================================================================================================

\newcommand{\includebib}[1] {
	\raggedright
	\bibliographystyle{plain}
	\bibliography{#1}
}

%=======================================================================================================================================================================================================

\endinput
